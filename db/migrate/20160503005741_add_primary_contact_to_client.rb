class AddPrimaryContactToClient < ActiveRecord::Migration
  def change
    add_column :clients, :primary_contact_id, :integer
  end
end
