class CreateContacts < ActiveRecord::Migration
  def change
    create_table :contacts do |t|
      t.string :first_name
      t.string :last_name
      t.string :job_title
      t.string :email
      t.string :phone_number
      t.string :telegram_number
      t.boolean :is_sales_rep, null: false, default: false
      t.integer :sex, null: false, default: false
      t.references :client, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
