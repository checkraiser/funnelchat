class AddBillingContactToClient < ActiveRecord::Migration
  def change
    add_column :clients, :billing_contact_id, :integer
  end
end
