class CreatePlans < ActiveRecord::Migration
  def change
    create_table :plans do |t|
      t.string :name, null: false
      t.integer :max_chats_per_month
      t.integer :max_sales_reps
      t.decimal :price_gbp
      t.decimal :price_aed

      t.timestamps null: false
    end
  end
end
