class CreateSubscriptions < ActiveRecord::Migration
  def change
    create_table :subscriptions do |t|
      t.references :plan, index: true, foreign_key: true
      t.references :client, index: true, foreign_key: true
      t.datetime :started_at
      t.datetime :ended_at

      t.timestamps null: false
    end
  end
end
