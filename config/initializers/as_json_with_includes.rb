module ActiveRecord
  module Calculations
    def convert_activerecord_includes_to_json_includes(active_record_includes)
      return active_record_includes if active_record_includes.is_a? Symbol
      temp_arr = []
      temp_hash = {}
      if active_record_includes.is_a? Array
        active_record_includes.each do |item|
          temp_arr << convert_activerecord_includes_to_json_includes(item)
        end
        return temp_arr
      end
      if active_record_includes.is_a? Hash
        active_record_includes.each do |key, value|
          temp_hash[key] = {include: convert_activerecord_includes_to_json_includes(value)}
        end
        return temp_hash
      end
    end

    def as_json_with_includes(options)
      active_record_includes = options[:includes]
      if active_record_includes.nil?
        return as_json(options)
      end
      json_inclusions = convert_activerecord_includes_to_json_includes(active_record_includes)
      options[:include] = json_inclusions
      options[:includes] = nil
      return includes(active_record_includes).as_json(options)
    end
  end
end