class Client < ActiveRecord::Base
  validates :name, presence: true
  has_many :contacts
  has_one :plan

  def primary_contact
    Contact.find_by(id: primary_contact_id)
  end
  def billing_contact
    Contact.find_by(id: billing_contact_id)
  end

  def set_primary_contact!(contact)
    self.update! primary_contact_id: contact.id
  end

  def set_billing_contact!(contact)
    self.update! billing_contact_id: contact.id
  end


  def create_contact! contact_params
    create_contact_params = contact_params.slice(:first_name, :last_name, :job_title, :phone_number, :email,
                                                 :telegram_number, :sex)
    is_sales_rep = contact_params[:roles][:is_sales_rep]
    is_primary_contact = contact_params[:roles][:primary_contact]
    is_billing_contact = contact_params[:roles][:billing_contact]
    create_contact_params.merge!(is_sales_rep: is_sales_rep)
    ActiveRecord::Base.transaction do
      new_contact = self.contacts.create! create_contact_params
      self.set_primary_contact!(new_contact) if is_primary_contact
      self.set_billing_contact!(new_contact) if is_billing_contact
      TelegramContact.create new_contact
    end

    self
  end

  def update_contact! contact, contact_params
    update_contact_params = contact_params.slice(:first_name, :last_name, :job_title, :phone_number, :email,
                                                 :telegram_number, :sex)
    is_sales_rep = contact_params[:roles][:is_sales_rep]
    is_primary_contact = contact_params[:roles][:primary_contact]
    is_billing_contact = contact_params[:roles][:billing_contact]
    update_contact_params.merge!(is_sales_rep: is_sales_rep)
    ActiveRecord::Base.transaction do
      contact.update! update_contact_params
      if is_primary_contact
        self.set_primary_contact!(contact)
      elsif self.primary_contact_id == contact.id
        self.update! primary_contact_id: nil
      end
      if is_billing_contact
        self.set_billing_contact!(contact)
      elsif self.billing_contact_id == contact.id
        self.update! billing_contact_id: nil
      end
      TelegramContact.create contact
    end
    self
  end
end
