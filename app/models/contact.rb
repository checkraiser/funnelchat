class Contact < ActiveRecord::Base
  belongs_to :client

  validates :client, presence: true

  enum sex: [:male, :female]

  def name
    "#{first_name} #{last_name}"
  end
  def roles
    {
        primary_contact: client.primary_contact_id == id,
        billing_contact: client.billing_contact_id == id,
        is_sales_rep: is_sales_rep
    }
  end

end
