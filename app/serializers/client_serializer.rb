class ClientSerializer < ApplicationSerializer
  root false
  attributes :name, :url
  has_one :primary_contact, serializer: ContactSerializer
  has_one :billing_contact, serializer: ContactSerializer
  has_many :contacts
end