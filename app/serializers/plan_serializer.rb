class PlanSerializer < ApplicationSerializer
  root false
  attributes :name, :max_chats_per_month, :max_sales_reps, :price_gbp, :price_aed
end