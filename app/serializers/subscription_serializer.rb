class SubscriptionSerializer < ApplicationSerializer
  root false
  attributes :started_at, :ended_at
  has_one :client, serializer: ClientSerializer
  has_one :plan, serializer: PlanSerializer
end