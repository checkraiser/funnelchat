class ContactSerializer < ApplicationSerializer
  root false
  attributes :name, :first_name, :last_name, :job_title, :email, :phone_number, :telegram_number, :roles, :sex
end