class PlansController < ApplicationController
  def manage
    authorize :plan
    plans = Plan.all
    respond_to do |format|
      format.html {
        gon.plans = plans
      }
    end
  end
  def index
    authorize :plan
    plans = Plan.all
    render json: {plans: plans}
  end
  def create
    authorize :plan
    @plan = Plan.new(plan_params)
    save_resource @plan
  end

  def update
    @plan = Plan.find(params[:id])
    authorize @plan
    update_resource @plan, plan_params
  end
  protected
  def plan_params
    params.require(:plan).permit(:name, :max_chats_per_month, :max_sales_reps, :price_gbp, :price_aed)
  end

end