class ClientsController < ApplicationController
  def manage
    authorize :client
    clients = Client.all
    respond_to do |format|
      format.html {
        gon.clients = clients
      }
    end
  end

  def index
    authorize :client
    clients = Client.all
    render json: {clients: clients}
  end

  def show
    authorize :client
    client = Client.find(params[:id])
    gon.client = ClientSerializer.new(client)
  end
  def create
    @client = Client.new(client_params)
    authorize @client
    save_resource @client
  end

  def update
    @client = Client.find(params[:id])
    authorize @client
    update_resource @client, client_params
  end

  protected
  def client_params
    params.require(:client).permit(:name, :url)
  end
end