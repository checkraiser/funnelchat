class SubscriptionsController < ApplicationController
  def manage
    authorize :subscription
    subscriptions = Subscription.includes(:plan, :client)
    plans = Plan.all
    clients = Client.all
    respond_to do |format|
      format.html {
        gon.subscriptions = subscriptions.as_json(include: [:plan, :client])
        gon.plans = plans
        gon.clients = clients
      }
    end
  end
  def create
    authorize :subscription
    client_id = subscription_params[:client_id]
    plan_id = subscription_params[:plan_id]
    started_at = DateTime.strptime(subscription_params[:started_at],"%Y-%m-%d")
    ended_at = DateTime.strptime(subscription_params[:ended_at],"%Y-%m-%d")
    client = Client.find(client_id)
    plan = Plan.find(plan_id)
    @subscription = Subscription.new(client: client, plan: plan, started_at: started_at, ended_at: ended_at)
    save_resource @subscription
  end
  def update
    @subscription = Subscription.find(params[:id])
    authorize :subscription
    client_id = subscription_params[:client_id]
    plan_id = subscription_params[:plan_id]
    started_at = DateTime.strptime(subscription_params[:started_at],"%Y-%m-%d")
    ended_at = DateTime.strptime(subscription_params[:ended_at],"%Y-%m-%d")
    client = Client.find(client_id)
    plan = Plan.find(plan_id)

    @subscription.update! client: client,
                          plan: plan,
                          started_at: started_at,
                          ended_at: ended_at
    render json: @subscription

  end
  private
  def subscription_params
    params.require(:subscription).permit(:id, :client_id, :plan_id, :started_at, :ended_at)
  end
end