class ContactsController < ApplicationController
  before_action :set_client
  def index
    authorize :user
    contacts = @client.contacts
    render json: {contacts: contacts}
  end
  def create
    authorize :contact
    client = @client.create_contact! contact_params
    render json: ClientSerializer.new(client, {root: true})
  end

  def update
    @contact = @client.contacts.find(params[:id])
    authorize @contact
    client = @client.update_contact! @contact, contact_params
    render json: ClientSerializer.new(client, {root: true})
  end
  protected
  def set_client
    @client = Client.find(params[:client_id])
  end
  def contact_params
    params.require(:contact).permit(:first_name, :last_name, :job_title, :phone_number, :email,
                                    :telegram_number, :sex, :roles => [:primary_contact, :billing_contact, :is_sales_rep])
  end

end