module TelegramContact
  def self.create(contact)
    contact_serializer = ContactSerializer.new(contact)
    $redis.publish 'contact-created', contact.to_json
  end
end
