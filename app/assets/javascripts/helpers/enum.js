var app_enum = {}

/******************************************
 * user enumerations
 ******************************************/
app_enum.user = {}
app_enum.user.roles = ["guest", "admin"]

app_enum.contact = {}
app_enum.contact.sexes = ["male", "female"]