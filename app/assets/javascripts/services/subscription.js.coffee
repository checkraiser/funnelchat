app.factory 'Subscription', ['BaseModel', 'Api', (BaseModel, Api) ->
  self = BaseModel.new("subscription", "/subscriptions", ["id"])
  self.index = []
  self.init = (subscriptions)->
    self.index = subscriptions

  return self
]
