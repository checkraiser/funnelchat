app.factory 'Plan', ['BaseModel', 'Api', (BaseModel, Api) ->
  self = BaseModel.new("plan", "/plans", ["id"])
  self.index = []
  self.init = (plans)->
    self.index = plans

  return self
]
