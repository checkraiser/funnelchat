app.factory 'Client', ['BaseModel', 'Api', (BaseModel, Api) ->
  self = BaseModel.new("client", "/clients", ["id"])
  self.index = []
  self.init = (clients)->
    self.index = clients
  self.create_contact = (client_id, contact, options)->
    Api.post "/clients/" + client_id + "/contacts", {
      data: {contact: contact}
      button: options.button if options.button
      success_message: options.success_message if options.success_message
      success: (response)->
        options.success(response) if options.success
    }
  self.update_contact = (client_id, contact, options)->
    Api.put "/clients/" + client_id + "/contacts/" + contact.id, {
      data: {contact: contact}
      button: options.button if options.button
      success_message: options.success_message if options.success_message
      success: (response)->
        options.success(response) if options.success
    }
  return self
]
