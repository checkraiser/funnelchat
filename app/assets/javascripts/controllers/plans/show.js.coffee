app.controller 'clients_show_controller', ['$scope', 'Client', ($scope, Client) ->
  $scope.selected_contact =
    roles:
      primary_contact: false
      billing_contact: false
      is_sales_rep: false
  $scope.client = gon.client
  console.log $scope.client
  $scope.open_new_contact_modal = (e)->
    $scope.selected_contact =
      roles:
        primary_contact: false
        billing_contact: false
        is_sales_rep: false
    $('.new-contact-modal').modal('show')
    return

  $scope.open_settings_contact_modal = (contact, e)->
    $scope.selected_contact = deep_copy(contact)
    $('.settings-contact-modal').modal('show')
    return

  $scope.create_contact = (e)->
    Client.create_contact $scope.client.id, $scope.selected_contact, {
      button: $(e.currentTarget)
      success_message: "Contact created!"
      success: (response)->
        $scope.client = response.client
        $scope.selected_contact =
          roles:
            primary_contact: false
            billing_contact: false
            is_sales_rep: false
        $('.new-contact-modal').modal('hide')
        return
    }

  $scope.update_contact = (e)->
    Client.update_contact $scope.client.id, $scope.selected_contact, {
      button: $(e.currentTarget)
      success_message: "Contact updated!"
      success: (response)->
        $scope.client = response.client
        $scope.selected_contact =
          roles:
            primary_contact: false
            billing_contact: false
            is_sales_rep: false
        $('.settings-contact-modal').modal('hide')
        return
    }

]