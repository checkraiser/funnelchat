app.controller 'plans_manage_controller', ['$scope', 'Plan', ($scope, Plan) ->
  $scope.view_model =
    plan: {}
  $scope.Plan = Plan
  Plan.init(gon.plans)

  $scope.open_new_plan_modal = (e)->
    $scope.view_model.plan = {}
    $('.new-plan-modal').modal('show')
    return

  $scope.open_settings_plan_modal = (plan, e)->
    $scope.view_model.plan = deep_copy(plan)
    $('.settings-plan-modal').modal('show')
    return

  $scope.create_plan = (e)->
    Plan.create $scope.view_model.plan, {
      button: $(e.currentTarget)
      success_message: "Plan created!"
      success: (response)->
        $('.new-plan-modal').modal('hide')
        return
    }

  $scope.update_plan = (e)->
    Plan.update $scope.view_model.plan, {
      button: $(e.currentTarget)
      success_message: "Plan updated!"
      success: (response)->
        $('.settings-plan-modal').modal('hide')
        return
    }
]