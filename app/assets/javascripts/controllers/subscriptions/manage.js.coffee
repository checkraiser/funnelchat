app.controller 'subscriptions_manage_controller', ['$scope', 'Subscription', ($scope, Subscription) ->
  $scope.view_model =
    subscription = blank_hash
  $scope.Subscription = Subscription
  Subscription.init(gon.subscriptions)
  $scope.plans = gon.plans
  $scope.clients = gon.clients

  $scope.open_new_subscription_modal = (e)->
    $scope.view_model.subscription = blank_hash()
    $('.new-subscription-modal').modal('show')
    return

  $scope.open_settings_subscription_modal = (subscription, e)->
    $scope.view_model.subscription = deep_copy(subscription)
    $('.settings-subscription-modal').modal('show')
    return

  $scope.create_subscription = (e)->
    subscription_params =
      plan_id: $scope.view_model.subscription.plan.id
      client_id: $scope.view_model.subscription.client.id
      started_at: $scope.view_model.subscription.started_at
      ended_at: $scope.view_model.subscription.ended_at
    Subscription.create subscription_params, {
      button: $(e.currentTarget)
      success_message: "Subscription created!"
      success: (response)->
        $('.new-subscription-modal').modal('hide')
        return
    }

  $scope.update_subscription = (e)->
    subscription_params =
      id: $scope.view_model.subscription.id
      plan_id: $scope.view_model.subscription.plan.id
      client_id: $scope.view_model.subscription.client.id
      started_at: $scope.view_model.subscription.started_at
      ended_at: $scope.view_model.subscription.ended_at
    Subscription.update subscription_params, {
      button: $(e.currentTarget)
      success_message: "Subscription updated!"
      success: (response)->
        $('.settings-subscription-modal').modal('hide')
        return
    }
]