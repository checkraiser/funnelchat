app.directive "ngSelect", ['$templateCache', ($templateCache)->
  return {
    restrict: "A",
    templateUrl: "components/shared/ng_select.html"
    bindToController: true
    transclude: true
    controllerAs : 'ng_select'
    scope: {
      filter: "="
      placeholder: "@"
      filtered_items: "=filteredItems"
      selected_text: "=selectedText"
      selected_item: "=selectedItem"
      query: "="
    }
    controller: ["$timeout", ($timeout)->
      self = this
      self.is_empty = is_empty
      self.menu_visible = false

      if !is_empty(self.selected_text)
        self.query = self.selected_text

      self.update_items = ()->
        self.filtered_items.set_array(self.filter(self.query))

      self.update_items()

      self.display_text_clicked = (e)->
        self.menu_visible = true
        query_input = $(e.currentTarget).parent().find(".ng-select-query-input :text").first();
        $timeout ()->
          query_input.select()

      select_item = (item)->
        self.selected_item = item
        $timeout ()->
          self.query = self.selected_text

      self.item_clicked = (item)->
        select_item(item)

      self.input_keydown = (e) ->
        if e.keyCode == 40 # down arrow
          e.preventDefault()
          self.filtered_items.select_next()
          return
        if e.keyCode == 38 # up arrow
          e.preventDefault()
          self.filtered_items.select_prev()
          return
        if e.keyCode == 13
          select_item(self.filtered_items.selected)
          self.menu_visible = false
          e.preventDefault()

    ]
  }
]
