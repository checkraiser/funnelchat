require 'rails_helper'

describe PlansController do
  include_context :gon
  let!(:admin) { FactoryGirl.create(:user, :admin) }
  before do
    sign_in admin
  end

  describe "POST #create" do
    let(:plan_params) {
      {
          name: "MyString",
          max_chats_per_month: 1,
          max_sales_reps: 1,
          price_gbp: "9.99",
          price_aed: "9.99"
      }
    }
    it "returns updated plan data" do
      post :create, plan: plan_params
      expect(Plan.count).to eq 1
    end
  end
end