require 'rails_helper'

describe SubscriptionsController do
  include_context :gon
  let!(:admin) { FactoryGirl.create(:user, :admin) }
  let!(:client) { FactoryGirl.create(:client) }
  let!(:plan) { FactoryGirl.create(:plan) }
  before do
    sign_in admin
  end

  describe "POST #create" do
    let(:subscription_params) {
      {
          plan_id: plan.id,
          client_id: client.id,
          started_at: Time.new(2016, 10, 1),
          ended_at: Time.new(2016, 12, 1)
      }
    }
    it "returns updated subscription data" do
      post :create, subscription: subscription_params
      expect(Subscription.count).to eq 1
      subscription = Subscription.last
      expect(subscription.plan.id).to eq subscription_params[:plan_id]
      expect(subscription.started_at).to eq subscription_params[:started_at]
    end
  end
end