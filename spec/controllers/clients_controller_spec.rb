require 'rails_helper'

describe ClientsController do
  include_context :gon
  let!(:admin) { FactoryGirl.create(:user, :admin) }
  let!(:client) { FactoryGirl.create(:client) }
  before do
    sign_in admin
  end
  describe "GET #manage" do
    it "render manage template" do
      get :manage
      expect(response).to render_template(:manage)
    end
  end

  describe "GET #show" do
    it "render show template" do
      get :show, id: client.id
      expect(response).to render_template(:show)
    end
    it "returns gon.client data" do
      get :show, id: client.id
      expect(gon['client']['id']).to eq client.id
      expect(gon['client']['contacts']).to eq []
    end
  end


end