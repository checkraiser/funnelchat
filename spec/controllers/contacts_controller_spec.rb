require 'rails_helper'

describe ContactsController do
  include_context :gon
  let!(:admin) { FactoryGirl.create(:user, :admin) }
  let!(:client) { FactoryGirl.create(:client) }
  before do
    sign_in admin
  end

  describe "POST #create" do
    let(:contact_params) {
      {
          first_name: "test",
          last_name: "test",
          job_title: "CEO",
          phone_number: "123456",
          email: "test@test.com",
          telegram_number: "12345",
          sex: "male",
          roles: {
              primary_contact: true,
              billing_contact: true,
              is_sales_rep: true
          }
      }
    }
    it "returns updated client data" do
      post :create, client_id: client.id, contact: contact_params
      expect(client.contacts.count).to eq 1
    end
  end
end