FactoryGirl.define do
  factory :contact do
    first_name "MyString"
    last_name "MyString"
    job_title "MyString"
    email "MyString"
    phone_number "MyString"
    telegram_number "MyString"
    is_sales_rep false
    sex 1
    client
  end
end
