FactoryGirl.define do
  factory :subscription do
    plan nil
    client nil
    started_at "2016-05-06 15:24:50"
    ended_at "2016-05-06 15:24:50"
  end
end
