FactoryGirl.define do
  factory :plan do
    name "MyString"
    max_chats_per_month 1
    max_sales_reps 1
    price_gbp "9.99"
    price_aed "9.99"
  end
end
