require 'rails_helper'

RSpec.describe Client, type: :model do
  let!(:client) { FactoryGirl.create(:client) }
  it "has one primary contact" do
    contact = FactoryGirl.create(:contact, client: client)
    client.set_primary_contact!(contact)
    expect(client.primary_contact).to eq contact
  end
  it "update_contact!" do
    contact = FactoryGirl.create(:contact, client: client)
    contact_params =
        {
            first_name: "Truong",
            roles: {
                primary_contact: true,
                billing_contact: false,
                is_sales_rep: false
            }
        }
    client.update_contact! contact, contact_params
    contact = contact.reload
    expect(client.primary_contact).to eq contact
    expect(contact.roles).to eq contact_params[:roles]
  end
  # 0934929288
end
